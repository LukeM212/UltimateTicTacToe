// TODO: Use alerts for things like winning / losing / drawing and when other players cheat / leave

// Game client
var client = new GameClient("wss://lukesthings.com/games/UTTT");

// Game lobby
const menuHeight = 25, menuWidth = 190;
var menuScroll = 0;

// Game state
var grid, turn = "", square, me, opponent;
const lines = "012,345,678,036,147,258,048,246".split(',').map(l => l.split(''));
function placePiece(grid, x, y, size, pos, ...inner) {
	var pieceX = x + (pos % 3) * size / 3,
		pieceY = y + Math.floor(pos / 3) * size / 3;
	
	if (inner.length == 0 || placePiece(grid[pos], pieceX, pieceY, size / 3, ...inner)) {
		grid[pos] = turn;
		
		hctx.strokeStyle = "#909090";
		hctx.lineWidth = size / 30;
		hctx.beginPath();
		if (turn == "O") {
			hctx.arc(pieceX + size / 6, pieceY + size / 6, size * 4 / 30, 0, Math.PI * 2);
		} else {
			hctx.moveTo(pieceX + size  /30, pieceY + size  /30);
			hctx.lineTo(pieceX + size*9/30, pieceY + size*9/30);
			hctx.moveTo(pieceX + size  /30, pieceY + size*9/30);
			hctx.lineTo(pieceX + size*9/30, pieceY + size  /30);
		}
		hctx.stroke();
		
		var matches = lines.filter(l => l.every(p => grid[p] == turn));
		if (matches.length > 0) {
			hctx.beginPath();
			for (match of matches) {
				hctx.moveTo(x + (match[0] % 3) * size / 3 + size / 6,
					y + Math.floor(match[0] / 3) * size / 3 + size / 6);
				hctx.lineTo(x + (match[2] % 3) * size / 3 + size / 6,
					y + Math.floor(match[2] / 3) * size / 3 + size / 6);
			}
			hctx.stroke();
			
			return true;
		}
	}
	
	return false;
}

// Game canvas
var canvas = document.getElementById("GameCanvas"), hiddenCanvas = document.createElement('canvas');
var ctx = canvas.getContext('2d'), hctx = hiddenCanvas.getContext('2d');
function draw() {
	// Reset
	ctx.textAlign = "center";
	ctx.textBaseline = "middle";
	
	// Fill
	ctx.fillStyle = "#FFFFFF";
	ctx.fillRect(0, 0, 450, 450);
	
	switch (client.state.stage) {
		case "connecting":
			ctx.fillStyle = "#505050";
			ctx.fillText("Connecting to game server...", 225, 225);
			break;
		case "disconnected":
			ctx.fillStyle = "#505050";
			ctx.fillText("Lost connection, reload to try again", 225, 225);
			break;
		case "lobby":
			// Title
			ctx.fillStyle = "#505050";
			ctx.fillText("Games:", 225, 25);
			
			// Buttons
			ctx.drawImage(hiddenCanvas, 0, menuScroll, menuWidth - 20, 350, 130, 50, menuWidth - 20, 350);
			
			// Scroll bar
			var maxHeight = (Object.keys(client.state.data).length + 1) * menuHeight - 5;
			if (maxHeight > 350) {
				ctx.fillStyle = "#D0D0D0";
				ctx.fillRect(305, 50, 15, 350);
				ctx.fillStyle = "#909090";
				ctx.fillRect(305, 50 + menuScroll * 350 / maxHeight, 15, 350 * 350 / maxHeight);
			}
			
			// Hover
			if (curX >= 130 && curX < 300 && curY >= 50 && curY < 400) {
				ctx.fillStyle = "rgba(255,255,255,0.25)";
				var yPos = 50 - menuScroll;
				for (var room in client.state.data) {
					if (curY >= yPos && curY < yPos + 20)
						ctx.fillRect(130, yPos, menuWidth - 20, 20);
					yPos += menuHeight;
				}
				if (curX >= 150 && curY >= yPos && curY < yPos + 20)
					ctx.fillRect(150, yPos, menuWidth - 40, 20);
			}
			break;
		case "room":
			ctx.fillStyle = "#505050";
			ctx.fillText("Waiting for second player...", 225, 225);
			break;
		case "game":
			// Draw board
			ctx.drawImage(hiddenCanvas, 0, 0);
			
			// Hover
			if (turn == me) {
				for (var big = 0; big < 9; big++) {
					var x = (big % 3) * 150,
						y = Math.floor(big / 3) * 150;
					if (square & Math.pow(2, big)) {
						ctx.fillStyle = "rgba(0,255,255,0.25)";
						ctx.fillRect(x + 3, y + 3, 144, 144);
						ctx.fillStyle = "rgba(255,255,0,0.25)";
						if (curX >= x && curX < x + 150 && curY >= y && curY < y + 150)
							for (var small = 0; small < 9; small++) {
								var x2 = x + (small % 3) * 50,
									y2 = y + Math.floor(small / 3) * 50;
								if (grid[big][small] == "" && curX >= x2 && curX < x2 + 50 && curY >= y2 && curY < y2 + 50)
									ctx.fillRect(x2 + 3, y2 + 3, 44, 44);
							}
					}
				}
			}
			
			// Other player left
			if (client.state.data.length < 2) {
				ctx.fillStyle = "#505050";
				ctx.fillText("Other player left", 225, 225);
			}
			break;
	}
}

// Canvas mouse control
var startX, startY, prevX, prevY, curX, curY, mouseDrag = -1;
document.onmousedown = event => {
	if (mouseDrag < 0 && event.buttons & 1) {
		startX = prevX = curX = event.pageX - canvas.offsetLeft;
		startY = prevY = curX = event.pageY - canvas.offsetTop;
		mouseDrag = 0;
	}
};
document.onmouseup = event => {
	if (!(event.buttons & 1)) {
		if (mouseDrag >= 0 && mouseDrag < 2) {
			curX = event.pageX - canvas.offsetLeft;
			curY = event.pageY - canvas.offsetTop;
			canvasClick()
		}
		mouseDrag = -1;
	}
};
document.onmousemove = event => {
	curX = event.pageX - canvas.offsetLeft;
	curY = event.pageY - canvas.offsetTop;
	if (mouseDrag < 0) canvasMove();
	else if (++mouseDrag >= 2) canvasDrag();
	prevX = curX;
	prevY = curY;
};
function canvasMove() {
	draw();
}
function canvasClick() {
	if (client.state.stage == "lobby" && curX >= 130 && curX < 300 && curY >= 50 && curY < 400) {
		var yPos = 50 - menuScroll;
		for (var room in client.state.data) {
			if (curY >= yPos && curY < yPos + 20) {
				client.join(room);
				return;
			}
			yPos += menuHeight;
		}
		if (curX >= 150 && curY >= yPos && curY < yPos + 20) {
			var room = prompt("Room name: ");
			if (room != null) client.create(room, 2);
			return;
		}
	} else if (client.state.stage == "game" && turn == me) {
		for (var big = 0; big < 9; big++) {
			var x = (big % 3) * 150,
				y = Math.floor(big / 3) * 150;
			if ((square & Math.pow(2, big)) && curX >= x && curX < x + 150 && curY >= y && curY < y + 150)
				for (var small = 0; small < 9; small++) {
					var x2 = x + (small % 3) * 50,
						y2 = y + Math.floor(small / 3) * 50;
					if (grid[big][small] == "" && curX >= x2 && curX < x2 + 50 && curY >= y2 && curY < y2 + 50) {
						if (placePiece(grid, 0, 0, 450, big, small)) grid = turn;
						turn = {X:"O",O:"X"}[turn];
						square = Array.isArray(grid) ? Array.isArray(grid[small]) ? Math.pow(2, small) :
							grid.map((b, i) => Array.isArray(b) ? Math.pow(2, i) : 0).reduce((a, b) => a + b) : 0;
						client.send("turn", [big, small].join(""));
						draw();
					}
				}
		}
	}
}
function canvasDrag() {
	if (client.state.stage == "lobby") {
		var maxHeight = (Object.keys(client.state.data).length + 1) * menuHeight - 5;
		if (startX >= 130 && startX < 300)
			menuScroll -= curY - prevY;
		else if (startX >= 305 && startX < 320)
			menuScroll += ((curY - prevY) / 350) * maxHeight;
		else return;
		menuScroll = Math.min(menuScroll, maxHeight - 350);
		menuScroll = Math.max(menuScroll, 0);
		draw();
	}
}

// Client messages
client.onStateChanged = () => {
	if (client.state.stage == "lobby") {
		// TODO: Move into gameLib, and allow for custom list, size, buttons, etc
		
		// Set up canvas
		hiddenCanvas.width = menuWidth - 20;
		hiddenCanvas.height = (Object.keys(client.state.data).length + 1) * menuHeight - 5;
		hctx.textAlign = "center";
		hctx.textBaseline = "middle";
		hctx.fillStyle = "#FFFFFF";
		hctx.fillRect(0, 0, hiddenCanvas.width, hiddenCanvas.height);
		
		// Draw button function
		var yPos = 0;
		function drawButton(text, players, max) {
			hctx.fillStyle = "#D0D0D0";
			hctx.fillRect(players ? 0 : 20, yPos, hiddenCanvas.width - (players ? 0 : 20), 20);
			hctx.fillStyle = "#000000";
			hctx.fillText(text, menuWidth / 2, yPos + 10);
			if (players) hctx.fillText(players + "/" + max || "∞", 10, yPos + 10);
			yPos += menuHeight;
		}
		
		// Draw buttons
		for (var room in client.state.data)
			drawButton(room, client.state.data[room].players, client.state.data[room].max);
		drawButton("Create New");
	} else if (client.state.stage == "room" && !client.state.data[client.state.myId].ready && Object.keys(client.state.data).length == 2) {
		client.ready();
	} else if (client.state.stage == "game" && !grid) {
		// Set up canvas
		hiddenCanvas.width = hiddenCanvas.height = 450;
		hctx.textAlign = "center";
		hctx.textBaseline = "middle";
		hctx.fillStyle = "#FFFFFF";
		hctx.fillRect(0, 0, 450, 450);
		
		// Draw and set up grid
		hctx.fillStyle = "#D0D0D0";
		function drawGrid(x, y, size) {
			var width = Math.floor(size / 150) * 2;
			hctx.fillRect(x + size  /3 - width/2, y + width*4, width, size - width*8);
			hctx.fillRect(x + size*2/3 - width/2, y + width*4, width, size - width*8);
			hctx.fillRect(x + width*4, y + size  /3 - width/2, size - width*8, width);
			hctx.fillRect(x + width*4, y + size*2/3 - width/2, size - width*8, width);
		}
		grid = [ ];
		drawGrid(0, 0, 450);
		for (var big = 0; big < 9; big++) {
			grid.push([ ]);
			drawGrid((big % 3) * 150, Math.floor(big / 3) * 150, 150);
			for (var small = 0; small < 9; small++)
				grid[big].push("");
		}
		
		// Set up turn
		square = 0b111111111;
		turn = "X";
		me = ["X", "O"][client.state.myId];
		opponent = ["O", "X"][client.state.myId];
	}
	draw();
};
client.onMessage = (sender, type, ...msg) => {
	if (type == "turn") {
		if (turn != opponent || msg.length != 1 || !/^[0-8]{2}$/.test(msg[0]) || !(square & Math.pow(2, msg[0][0])) || grid[msg[0][0]][msg[0][1]] != "") {
			client.leave(); // Opponent cheated
			grid = undefined;
			turn = "";
			square = undefined;
			me = undefined;
			opponent = undefined;
			client.onStateChanged();
		}
		else {
			if (placePiece(grid, 0, 0, 450, msg[0][0], msg[0][1])) grid = turn;
			turn = {X:"O",O:"X"}[turn];
			square = Array.isArray(grid) ? Array.isArray(grid[msg[0][1]]) ? Math.pow(2, msg[0][1]) :
				grid.map((b, i) => Array.isArray(b) ? Math.pow(2, i) : 0).reduce((a, b) => a + b) : 0;
			draw();
		}
	}
};

draw();
