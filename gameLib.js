/*
MESSAGES:
	LB - lobby messages
		RECIEVE:
			ADD - new room - id, players, max
			DAT - room data - id, players
			RMV - room closed - id
	RM - room messages
		SEND:
			NEW - new room - id, max, hidden
			ADD - join room - id
			RMV - leave room - (none)
			RDY - ready to start - (none)
		RECIEVE:
			SCC - created or joined room - myId
			ERR - could not join room - reason
			ADD - player joined game - id, ready
			DAT - player data - id, ready
			RMV - player left game - id
			SRT - game started - (none)
*/

function GameClient(url) {
	// Private variables
	var socket;
	
	// Public variables
	//this.onStateChanged = callback();
	//this.onMessage = callback(sender, type, ...msg);
	this.state = { stage: "connecting" };
	
	// Private functions
	var send = (...msg) => socket.send(msg.join('|'));
	var callback = (cback, ...args) => { if (cback) cback(...args) }
	
	// Public functions
	this.create = function(id, max = 0, hidden = false) {
		if (this.state.stage != "lobby")
			throw new Error("Already in room");
		else if (this.state.joining)
			throw new Error("Already joining room");
		else if (id.length == 0 || id.includes('|'))
			throw new Error("Invalid id");
		else if (!isFinite(max) || max < 0 || Math.floor(max) != max)
			throw new Error("Invalid max");
		else {
			this.state.joining = true;
			send("_RM-NEW", id, max, hidden != "false" && !!hidden);
		}
	};
	this.join = function(id) {
		if (this.state.stage != "lobby")
			throw new Error("Already in room");
		else if (this.state.joining)
			throw new Error("Already joining room");
		else if (id.length == 0 || id.includes('|'))
			throw new Error("Invalid id");
		else {
			this.state.joining = true;
			send("_RM-ADD", id);
		}
	};
	this.ready = function() {
		if (this.state.stage == "lobby")
			throw new Error("Not in a room");
		else if (this.state.data[this.state.myId].ready)
			throw new Error("Already ready");
		else {
			this.state.data[this.state.myId].ready = true;
			send("_RM-RDY");
		}
	};
	this.leave = function() {
		if (this.state.stage == "lobby")
			throw new Error("Not in a room");
		else {
			this.state = { stage: "lobby", data: { } };
			send("_RM-RMV");
		}
	};
	this.send = function(type, ...msg) {
		if (this.state.stage == "lobby")
			throw new Error("Not in a room");
		else if (!type || type.length == 0 || type.startsWith('_') || type.includes('|'))
			throw new Error("Invalid type");
		else if (msg.some(m => m.includes('|')))
			throw new Error("Invalid message");
		else send(type, ...msg);
	};
	
	// Socket
	socket = new WebSocket(url);
	socket.onopen = event => {
		this.state = { stage: "lobby", data: { } };
		console.log("Connection opened");
		callback(this.onStateChanged);
	};
	socket.onclose = event => {
		this.state = { stage: "disconnected" };
		console.log("Connection closed, code: " + event.code + ", reason: " + event.reason);
		callback(this.onStateChanged);
	};
	socket.onmessage = event => {
		var msg = event.data.split('|');
		if (msg.length == 0) return;
		
		if (msg[0].startsWith('_')) {
			switch (msg[0]) {
				case "_LB-ADD":
					this.state.data[msg[1]] = { players: Number(msg[2]), max: Number(msg[3]) };
					break;
				case "_LB-DAT":
					this.state.data[msg[1]].players = Number(msg[2]);
					break;
				case "_LB-RMV":
					delete this.state.data[msg[1]];
					break;
				case "_RM-SCC":
					this.state = { stage: "room", data: { }, myId: msg[1] };
					this.state.data[msg[1]] = { ready: false };
					break;
				case "_RM-ERR":
					console.log("Error joining room: " + msg[1]);
					delete this.state.joining;
					break;
				case "_RM-ADD":
					this.state.data[msg[1]] = { ready: msg[2] == "true" };
					break;
				case "_RM-DAT":
					this.state.data[msg[1]].ready = msg[2] == "true";
					break;
				case "_RM-SRT":
					this.state.stage = "game";
					this.state.data = Object.keys(this.state.data);
					break;
				case "_RM-RMV":
					if (this.state.stage == "room")
						delete this.state.data[msg[1]];
					else this.state.data.splice(this.state.data.indexOf(msg[1]), 1);
					break;
			}
			callback(this.onStateChanged);
		} else callback(this.onMessage, ...msg);
	};
}
